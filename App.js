import React from "react";
import { StyleSheet, View, FlatList, AsyncStorage } from "react-native";

import Search from "./components/Search";
import ImageItem from "./components/ImageItem";

const apiKey = "0fb35990e5801dfa7a62e1e9466ccd25";

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      photos: [],
      searchText: "",
      page: 1
    };
  }

  loadImages(searchText) {
    const url = `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${apiKey}&format=json&nojsoncallback=1&safe_search=1&text=${searchText}`;

    this.setState({ searchText });
    this.setState({ page: 1 });

    fetch(url)
      .then(res => res.json())
      .then(json => {
        const photos = json.photos.photo;
        this.setState({ photos });
      })
      .catch(error => {
        console.log(error);
      });
  }

  loadMoreImages() {
    let { page, searchText } = this.state;
    page++;
    const url = `https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=${apiKey}&format=json&nojsoncallback=1&safe_search=1&text=${searchText}&page=${page}`;

    this.setState({ page });

    fetch(url)
      .then(res => res.json())
      .then(json => {
        const photos = [...this.state.photos, ...json.photos.photo];
        this.setState({ photos });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    const { photos } = this.state;

    return (
      <View style={styles.container}>
        <Search onChange={text => this.loadImages(text)} />
        <FlatList
          data={photos}
          contentContainerStyle={styles.imagesList}
          keyExtractor={(item, index) => item.id + item.secret}
          numColumns={3}
          onEndReached={() => this.loadMoreImages()}
          renderItem={({ item }) => <ImageItem data={item} />}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "flex-start",
    justifyContent: "center",
    paddingTop: 40
  },
  imagesList: {}
});
