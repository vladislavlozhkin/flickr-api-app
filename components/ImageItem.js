import React from "react";
import { Image, StyleSheet } from "react-native";

function getImageUrl(data) {
  const { farm, server, id, secret } = data;
  const uri = `https://farm${farm}.static.flickr.com/${server}/${id}_${secret}.jpg`;

  return uri;
}

export default function ImageItem({ data }) {
  return <Image source={{ uri: getImageUrl(data) }} style={styles.image} />;
}

const styles = StyleSheet.create({
  image: {
    width: "33%",
    height: 100,
    borderWidth: 1,
    borderColor: "#000"
  }
});
