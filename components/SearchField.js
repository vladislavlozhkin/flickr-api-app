import React from "react";
import { TextInput, StyleSheet } from "react-native";

export default function SearchField({ onSubmit, value }) {
  return (
    <TextInput
      style={styles.searchField}
      onSubmitEditing={event => onSubmit(event.nativeEvent.text)}
      defaultValue={value}
      placeholder="Please enter here"
    />
  );
}

const styles = StyleSheet.create({
  searchField: {
    height: 40,
    borderColor: "gray",
    borderWidth: 1,
    width: "100%",
    paddingLeft: 10,
    paddingRight: 10
  }
});
