import React from "react";
import { StyleSheet, Picker } from "react-native";

export default function SearchHistory({ history, selected, onChange }) {
  return (
    <Picker
      selectedValue={selected}
      style={styles.picker}
      onValueChange={(value, index) => onChange(value, history[index][1])}
    >
      {history.map(item => (
        <Picker.Item label={item[1]} value={item[0]} key={item[0]} />
      ))}
    </Picker>
  );
}

const styles = StyleSheet.create({
  picker: {
    width: "100%",
    height: 50
  }
});
