import React from "react";
import { View, StyleSheet, AsyncStorage } from "react-native";

import SearchField from "./SearchField";
import SearchHistory from "./SearchHistory";

function getRandStr() {
  return (
    Math.random()
      .toString(36)
      .substring(2, 15) +
    Math.random()
      .toString(36)
      .substring(2, 15)
  );
}

export default class Search extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [],
      selected: {}
    };
  }

  componentDidMount() {
    this.loadHistory();
  }

  loadHistory() {
    AsyncStorage.getAllKeys((err, keys) => {
      AsyncStorage.multiGet(keys, (err, stores) => {
        this.setState({ history: stores });

        if (!this.state.selected.key && stores.length > 0) {
          this.setSelected(
            stores[stores.length - 1][0],
            stores[stores.length - 1][1]
          );
        }
      });
    });
  }

  addToHistory(value) {
    const key = getRandStr();

    AsyncStorage.setItem(key, value, err => {
      this.loadHistory();
      this.setSelected(key, value);
    });
  }

  onSubmitInput(text) {
    this.addToHistory(text);
  }

  setSelected(key, value) {
    this.setState({ selected: { key, value } });
    this.props.onChange(value);
  }

  render() {
    const { history, selected } = this.state;

    return (
      <View style={styles.container}>
        <SearchField
          onSubmit={text => this.onSubmitInput(text)}
          value={selected.value}
        />

        {history.length > 0 ? (
          <SearchHistory
            history={history}
            selected={selected.key}
            onChange={(key, value) => this.setSelected(key, value)}
          />
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    paddingLeft: 20,
    paddingRight: 20
  }
});
